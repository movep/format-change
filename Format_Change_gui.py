# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Format_Change.ui'
#
# Created by: PyQt5 UI code generator 5.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
import sys, time, os

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(812, 266)
        
        self.header = ['1']
        
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.tableWidget = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget.setGeometry(QtCore.QRect(10, 100, 791, 111))
        self.tableWidget.setRowCount(1)
        self.tableWidget.setColumnCount(1)
        self.tableWidget.setObjectName("tableWidget")
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setItem(0, 0, item)
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(10, 50, 231, 16))
        self.label.setObjectName("label")
        self.spinBox = QtWidgets.QSpinBox(self.centralwidget)
        self.spinBox.setEnabled(False)
        self.spinBox.setGeometry(QtCore.QRect(110, 70, 51, 22))
        self.spinBox.setMinimum(1)
        self.spinBox.setMaximum(999)
        self.spinBox.setProperty("value", 1)
        self.spinBox.setObjectName("spinBox")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(310, 0, 61, 16))
        self.label_2.setObjectName("label_2")
        self.lineEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit.setGeometry(QtCore.QRect(310, 20, 61, 22))
        self.lineEdit.setObjectName("lineEdit")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(390, 0, 131, 16))
        self.label_3.setObjectName("label_3")
        self.lineEdit_2 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_2.setGeometry(QtCore.QRect(390, 20, 271, 22))
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setEnabled(False)
        self.pushButton.setGeometry(QtCore.QRect(700, 60, 93, 28))
        self.pushButton.setObjectName("pushButton")
        self.pushButton.clicked.connect(lambda: gen(self.tableWidget, self.lineEdit_3, self.lineEdit_2, self.lineEdit, self.spinBox, self.comboBox))
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(700, 30, 93, 28))
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_2.clicked.connect(lambda: add_c(self.header, self.tableWidget))
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(10, 0, 101, 16))
        self.label_4.setObjectName("label_4")
        self.lineEdit_3 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_3.setGeometry(QtCore.QRect(10, 20, 271, 22))
        self.lineEdit_3.setReadOnly(True)
        self.lineEdit_3.setObjectName("lineEdit_3")
        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setGeometry(QtCore.QRect(700, 0, 93, 28))
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_3.clicked.connect(lambda: open_file(self.lineEdit_3, self.pushButton))
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(390, 50, 61, 16))
        self.label_5.setObjectName("label_5")
        self.comboBox = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox.setGeometry(QtCore.QRect(450, 50, 211, 22))
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.checkBox = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBox.setGeometry(QtCore.QRect(10, 70, 81, 20))
        self.checkBox.setObjectName("checkBox")
        self.checkBox.stateChanged.connect(lambda: state_changed(self.checkBox, self.spinBox))
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 812, 26))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Format Change"))
        __sortingEnabled = self.tableWidget.isSortingEnabled()
        self.tableWidget.setSortingEnabled(False)
        item = self.tableWidget.item(0, 0)
        item.setText(_translate("MainWindow", "[1]"))
        self.tableWidget.setSortingEnabled(__sortingEnabled)
        self.label.setText(_translate("MainWindow", "The number of column from source file"))
        self.label_2.setText(_translate("MainWindow", "Splite by"))
        self.lineEdit.setText(_translate("MainWindow", ","))
        
        tt = time.strftime("%Y %b %d-%H-%M-%S", time.localtime()) 
        file_name ='Simon_Mega_%s.mrp' % tt 
        self.lineEdit_2.setText(_translate("MainWindow", file_name))
        
        self.label_3.setText(_translate("MainWindow", "Destination file name"))
        self.pushButton.setText(_translate("MainWindow", "Generate"))
        self.pushButton_2.setText(_translate("MainWindow", "Add Column"))
        self.label_4.setText(_translate("MainWindow", "Source file Path"))
        self.pushButton_3.setText(_translate("MainWindow", "Open File"))
        self.label_5.setText(_translate("MainWindow", "Encoding:"))
        self.comboBox.setItemText(0, _translate("MainWindow", "GB2312"))
        self.comboBox.setItemText(1, _translate("MainWindow", "utf-8"))
        self.comboBox.setItemText(2, _translate("MainWindow", "utf-8-sig"))
        self.comboBox.setItemText(3, _translate("MainWindow", "Big5"))
        self.checkBox.setText(_translate("MainWindow", "Specify"))

def open_file(ledit, pb):
    options = QtWidgets.QFileDialog.Options()
    filePath, _ = QtWidgets.QFileDialog.getOpenFileName(QtWidgets.QWidget(),"QFileDialog.getOpenFileName()", "","All Files (*);;CSV Files (*.csv);;MRP Files (*.mrp)", options=options)

    if filePath != '':
        ledit.setText(filePath)
        pb.setEnabled(True)
        
def add_c(hd, table):
    value = len(hd) + 1
    hd.append(str(value))

    for i in range(len(hd)):
        item = QtWidgets.QTableWidgetItem()
        item.setText(hd[i])
        table.setHorizontalHeaderItem(i, item)
        
    table.setColumnCount(len(hd))
        
def gen(table, sedit, wedit, spc, spnum, cb):

    filePath = sedit.text()
    code = cb.currentText()
    print(code)
    
    try:
        if filePath:
            writeFile = wedit.text()
            fd = os.open(writeFile, os.O_RDWR|os.O_CREAT )
            
            with open(filePath, 'r', encoding = code) as rfile:
                with os.fdopen(fd, 'w') as new_file:
                    for line in rfile:
                        if line.find('[') != -1:
                            continue
                            #new_file.write(line)
                        elif line.find(']') != -1:
                            continue
                        elif line.find('#') != -1:
                            continue
                            #new_file.write(line)
                        else:
                            try:
                                l = [x.strip() for x in line.split(spc.text())]
                                if False == spnum.isEnabled() or spnum.value() == len(l):
                                    for cnum in range(table.columnCount()):
                                        it = table.item(0, cnum)
                                        if None == it:
                                            cv = ""
                                        else:
                                            cv = it.text()

                                        if len(cv) > 0:
                                            cs, suc = getc(cv)
                                        else:
                                            cs = ""
                                            suc = False
                                            
                                        if suc:
                                            temp = l[cs-1]
                                        else:
                                            temp = cs

                                        if cnum == table.columnCount()-1:
                                            new_file.write(temp + "\n")
                                        else:
                                            new_file.write(temp + spc.text() + " ")
                                            
                            except Exception as e:
                                QtWidgets.QMessageBox.about(QtWidgets.QWidget(), "Error", str(e))
            QtWidgets.QMessageBox.about(QtWidgets.QWidget(), "Done", "Generate %s is done."%writeFile)
    except Exception as e:
        QtWidgets.QMessageBox.about(QtWidgets.QWidget(), "Open Error", str(e))
                        

def getc(s):
    if s[0] == '[' and s[-1] == ']':
        try:
            l = len(s)
            col = int(s[1:l-1])
            return col, True
        except:
            return s, False
    else:
        return s, False

def state_changed(check, sp):
    if check.isChecked():
        sp.setEnabled(True)
    else:
        sp.setEnabled(False)