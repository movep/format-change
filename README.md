The tool will change source format to the other format. It usually uses with .csv file.

==Usage Example==

1. Open source file by ：Open File；.

2. (Optional) If source have 7 columns, please check "Specify" checkbox and then set ：The number of column from source file； to ．7・.

E.g.

0     ,0     ,0     ,0     ,0     ,0     ,0  
   
18    ,154   ,0     ,204   ,5     ,0     ,2.55

3. (Optional) Set "Splite by", the default is ','

4. If destination file have 2 columns, please ：Add Column；

5. If you want destination column 1 to copy from source column 4 (1 based), please set column 1 to [4].

6. If you want destination column 2 is "Simon", please set column 2 to Simon.

7. (Optional) select "Encoding", the default is "GB2312"

8. Click ：Generate； button.

The destination file in this example will be:

0, Simon

204, Simon

Note: ．[．, ．]・, ．#・ will skip that line to translate format.
�@�@
