import Format_Change_gui
from PyQt5 import QtCore, QtGui, QtWidgets
import sys

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    MWindow = QtWidgets.QMainWindow()
    ui = Format_Change_gui.Ui_MainWindow()
    ui.setupUi(MWindow)
    MWindow.show()
    sys.exit(app.exec_())